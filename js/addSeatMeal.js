/***
 * 添加套菜相关JS
 */

/***
 * 获取商户菜品列表
 */
layui.use(['table', 'form', 'element', 'upload'], function() {
	var table = layui.table,
		form = layui.form,
		element = layui.element,
		$ = layui.$,
		upload = layui.upload,
		laypage = layui.laypage;
	var foodGroupArray = []; //套菜分组的集合，包含套菜菜品
	var foodGroupMap = {};
	var groupId = -1;
	var seatMealFoodTableIns; //套菜中绑定的菜品表格
	var currentGroupMap = {}; //当前的分组
	var applyRangeData = []; //适用范围选中的值
	applyRangeData.push("TS");
	applyRangeData.push("KC");
	applyRangeData.push("WM");
	applyRangeData.push("WX");
	$.ajax({
		url: '/merchantfood/getFoodCategoryTree',
		type: "post",
		async: false,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			categoryData = data;
		}
	});
	$('#js-foodList').on('click', '.foodItemPanel', function() {
		var _this = $(this);
		if(_this.hasClass("current")) {
			_this.removeClass("current");
		} else {
			_this.addClass("current");
		}
	});
	element.on('nav(groupFilter)', function(elem) {
		console.log(elem); //得到当前点击的DOM对象
		for(var i in foodGroupArray) {
			var foodGroupData = foodGroupArray[i];
			if(foodGroupData.id == elem[0].id) {
				console.log(elem[0].id);
				currentGroupMap = foodGroupData;
				break;
			}
		}
		console.log('currentGroupMap:' + JSON.stringify(currentGroupMap));
		seatMealFoodTableIns.reload({
			data: currentGroupMap.merchantFoodGroupListEntity
		});
	});
	//套菜菜品属性提交
	form.on('submit(saveFoodAttrFilter)', function(data) {
		console.log(data.field);
		var paramMap = data.field;
		if(!isExistValInObj(paramMap, 'isinfluenceprice')) {
			paramMap['isinfluenceprice'] = 0;
		}
		if(!isExistValInObj(paramMap, 'isdefault')) {
			paramMap['isdefault'] = 0;
		}
		if(!isExistValInObj(paramMap, 'ismoreselect')) {
			paramMap['ismoreselect'] = 0;
		}
		//绑定的数据需要更新
		var groupFoodList = currentGroupMap.merchantFoodGroupListEntity;
		for(var i in groupFoodList) {
			var groupFoodData = groupFoodList[i];
			if(groupFoodData.id == paramMap.id) {
				var newFoodData = $.extend({}, groupFoodData, paramMap);
				groupFoodList[i] = newFoodData;
				console.log(groupFoodList);
				break;
			}
		}
		console.log(currentGroupMap);
		seatMealFoodTableIns.reload({
			data: groupFoodList
		});
		updateGroupFood(currentGroupMap);
		sealMealActive['closeIndexModal'].call();
	});
	/** 保存分组 */
	form.on('submit(saveFoodGroupSubmitFilter)', function(data) {
		var seatCategoryMap = data.field;
		console.log(seatCategoryMap);
		if(seatCategoryMap.category != 2) {
			if(isExistValInObj(seatCategoryMap, 'islimit')) {
				delete seatCategoryMap.islimit;
			}
			if(isExistValInObj(seatCategoryMap, 'staticqtybig')) {
				delete seatCategoryMap.staticqtybig;
			}
			if(isExistValInObj(seatCategoryMap, 'staticqtysmall')) {
				delete seatCategoryMap.staticqtysmall;
			}
			delete seatCategoryMap.finalqty;
			delete seatCategoryMap.limitcategory;
		} else {
			if(!isExistValInObj(seatCategoryMap, 'islimit')) {
				seatCategoryMap['islimit'] = 0;
			}
			if(seatCategoryMap.limitcategory == 1) {
				if(isExistValInObj(seatCategoryMap, 'staticqtysmall')) {
					delete seatCategoryMap.staticqtysmall;
				}
				if(isExistValInObj(seatCategoryMap, 'staticqtybig')) {
					delete seatCategoryMap.staticqtybig;
				}
			} else if(seatCategoryMap.limitcategory == 2) {
				if(isExistValInObj(seatCategoryMap, 'finalqty')) {
					delete seatCategoryMap.finalqty;
				}
			}
		}
		console.log(seatCategoryMap);
		seatCategoryMap['id'] = groupId;
		groupId = groupId - 1;
		foodGroupMap = seatCategoryMap;
		foodGroupArray.push(foodGroupMap);
		var html = '<li class="layui-nav-item" id="' + foodGroupMap.id + '">';
		if(foodGroupMap.category == 0) {
			html += '<a href="javascript:;">' + foodGroupMap.name + '（必选）</a></li>';
		} else if(foodGroupMap.category == 1) {
			html += '<a href="javascript:;">' + foodGroupMap.name + '（单选）</a></li>';
		} else if(foodGroupMap.category == 2) {
			html += '<a href="javascript:;">' + foodGroupMap.name + '（多选）</a></li>';
		}
		$("#js-ul-group").append(html);
		element.render('nav', 'groupFilter');
		layer.close(modalIndex);
	});
	/* 菜品分类1改变 */
	form.on('select(groupCategoryChangeFilter1)', function(data) {
		var bigcategory = categoryData[0].children;
		var text1 = data.value;
		if(text1 == "请选择") {
			$("#group_category_2").empty();
			$("#group_category_2").append('<option value="请选择">-请选择-</option>');
		} else {
			for(var i = 0; i < categoryData[0].children.length; i++) {
				if(text1 == categoryData[0].children[i].id) {
					$("#group_category_2").empty();
					$("#group_category_2").append('<option value="请选择">-请选择-</option>');
					for(var j = 0; j < bigcategory[i].children.length; j++) {
						$("#group_category_2").append('<option  value=' + bigcategory[i].children[j].id + ' >' + bigcategory[i].children[j].name + '</option>');
					}
				}
			}
		}
		form.render('select', 'saveOrUpdateGroupFoodFilter');
	});
	//支付范围
	form.on('checkbox(groupApplyRangeFilter)', function(data) {
		if(data.value == "ts") {
			if(data.elem.checked == true) {
				applyRangeData.push("TS");
			} else {
				applyRangeData.splice($.inArray("TS", applyRangeData), 1);
			}
		}
		if(data.value == "kc") {
			if(data.elem.checked == true) {
				applyRangeData.push("KC");
			} else {
				applyRangeData.splice($.inArray("KC", applyRangeData), 1);
			}
		}
		if(data.value == "wm") {
			if(data.elem.checked == true) {
				applyRangeData.push("WM");
			} else {
				applyRangeData.splice($.inArray("WM", applyRangeData), 1);
			}
		}
		if(data.value == "wx") {
			if(data.elem.checked == true) {
				applyRangeData.push("WX");
			} else {
				applyRangeData.splice($.inArray("WX", applyRangeData), 1);
			}
		}
	});
	/* 启用开台预点复选框过滤 */
	form.on('checkbox(groupEnablePointFilter)', function(data) {
		if(data.elem.checked == true) {
			$("#js-span-group-prepareway").css("display", "inline-block");
		} else {
			$("#js-span-group-prepareway").css("display", "none");
		}
	});
	form.on('radio(groupEnablePointTypeFilter)', function(data) {
		$("#groupEnablePointType").val(data.value);
	});
	//普通图片上传
	var uploadInst = upload.render({
		elem: '#groupBtnUpload',
		url: '/image/image/png',
		choose: function(obj) {
			console.log(obj);
		},
		done: function(res, index, upload) {
			if(res.code == 0) {
				$.ajax({
					type: "post",
					url: "/image/delete/" + $("#js-img-group-food").attr("data-src"),
					async: false,
					success: function(msg) {}
				});
				var html = '<img src="foodimage/png/' + res.image + '" id="js-img-group-food" data-src="' + res.image + '" style="width: 100%;height: 100%;" />';
				$("#groupBtnUpload").html(html);
			}
		}
	});
	/** 可变数量的单选框 **/
	form.on('radio(limitcategoryFilter)', function(data) {
		console.log(data.elem); //得到radio原始DOM对象
		console.log(data.value); //被点击的radio的value值
		if(data.value == 1) {
			$("#js-qty-panel").show();
			$("#js-minAndmaxQty-panel").hide();
		} else {
			$("#js-qty-panel").hide();
			$("#js-minAndmaxQty-panel").show();
		}
	});
	var sealMealActive = {
		addSetMealModal: function(othis) {
			foodGroupArray = [];
			document.getElementById("saveOrUpdateGroupFoodForm").reset();
			$("#js-groupFood-id").val("");
			table.clearCacheData('js-table-seatMealFood');
			$(".groupCheckedDefault").attr('checked', false);
			$("#groupEnablePointType").val("");
			$("#js-span-group-prepareway").hide();
			$.ajax({
				type: 'POST',
				url: '/merchantfood/getMaxCode',
				success: function(data) {
					$("#js-input-group-code").val(data);
					$("#js-input-group-barcode").val(data);
				}
			});
			foodFY();
			foodGroupMap = {
				'id': 1001,
				'name': '默认分组',
				'category': 0
			}
			currentGroupMap = foodGroupMap;
			foodGroupArray.push(foodGroupMap);
			console.log('添加菜品：' + JSON.stringify(foodGroupArray))
			initTable();
			var height = $(document).height();
			console.log('height:' + height);
			if(height > 645) {
				//$(".css-seatMeal-panel").css("height", "598px");
				$(".setMealGroupPanel").css("height", "538px")
				showModal('ADD-SETMEAL-MODAL', '添加套餐', '850px', $('#saveOrUpdateSetMealPanel'));
			} else {
				console.log('height:' + height);
				var str = (String)(height - 40);
				$(".setMealGroupPanel").css("height", (height - 270) + 'px');
				showModal('ADD-SETMEAL-MODAL', '添加套餐', ['850px', str + 'px'], $('#saveOrUpdateSetMealPanel'));
			}
		},
		modifyMerchantFoodModal: function(othis) {
			var checkStatus = table.checkStatus('tableMerchantFoodList');
			var checkData = checkStatus.data;
			if(checkData.length != 1) {
				layer.msg("编辑只能选中一行");
				return;
			}
			if(checkData[0].isgroup == 1) {
				document.getElementById("saveOrUpdateGroupFoodForm").reset();
				$("#js-groupFood-id").val(checkData[0].id);
				foodFY();
				$("#js-input-group-code").val(checkData[0].code);
				$("#js-input-group-name").val(checkData[0].name);
				$("#js-input-group-pinyin").val(checkData[0].pinyin);
				$("#js-input-group-barcode").val(checkData[0].barcode);
				$("#js-input-group-title").val(checkData[0].title);
				$("#js-input-group-retailPrice").val(checkData[0].retailPrice);
				$("#js-input-group-cost").val(checkData[0].cost);
				$("#js-input-group-vipPrice").val(checkData[0].vipPrice);
				$("#js-input-group-weixin").val(checkData[0].weixin);
				$("#js-input-group-deduct").val(checkData[0].deduct);
				$("#js-img-group-food").attr("src", "foodimage/png/" + checkData[0].avator);
				$("#js-input-group-desc").val(checkData[0].remark);
				$("#groupIscurrentPrices").attr("checked", checkData[0].iscurrentPrices == null || checkData[0].iscurrentPrices == 0 ? false : true);
				$("#groupIsweigh").attr("checked", checkData[0].isweigh == null || checkData[0].isweigh == 0 ? false : true);
				$("#groupForbiddiscount").attr("checked", checkData[0].forbiddiscount == null || checkData[0].forbiddiscount == 0 ? false : true);
				$("#groupEnablePoint").attr("checked", checkData[0].isprepare == null || checkData[0].isprepare == 0 ? false : true);
				if(checkData[0].isprepare == 1) {
					$("#js-span-group-prepareway").css("display", "inline-block");
					if(checkData[0].prepareway != null) {
						$("input[name='groupEnablePointType'][value=" + checkData[0].prepareway + "]").attr("checked", true);
					}
				}
				$("#js-input-group-num").val(checkData[0].prepareqty);
				$("#js-checkbox-status").attr("checked", checkData[0].status == 0 ? true : false);
				$("#js-input-group-status").val(checkData[0].status);
				$("#group_category_2").val(checkData[0].category);
				var applyRange = checkData[0].applyRange;
				if(applyRange != null) {
					if(applyRange.indexOf("TS") >= 0) {
						$("#js-checkbox-group-ts").attr("checked", true);
					} else {
						$("#js-checkbox-group-ts").attr("checked", false);
					}
					if(applyRange.indexOf("KC") >= 0) {
						$("#js-checkbox-group-kc").attr("checked", true);
					} else {
						$("#js-checkbox-group-kc").attr("checked", false);
					}
					if(applyRange.indexOf("WM") >= 0) {
						$("#js-checkbox-group-wm").attr("checked", true);
					} else {
						$("#js-checkbox-group-wm").attr("checked", false);
					}
					if(applyRange.indexOf("WX") >= 0) {
						$("#js-checkbox-group-wx").attr("checked", true);
					} else {
						$("#js-checkbox-group-wx").attr("checked", false);
					}
				} else {
					$(".group-checked-false").attr("checked", false);
				}
				var category_big = "";
				if(categoryData[0] != null && categoryData[0].children != null) {
					for(var i = 0; i < categoryData[0].children.length; i++) {
						for(var j = 0; j < categoryData[0].children[i].children.length; j++) {
							if(checkData[0].category == categoryData[0].children[i].children[j].id) {
								category_big = categoryData[0].children[i].id;
								break;
							}
						}
					}
				}
				for(var i = 0; i < categoryData[0].children.length; i++) {
					if(category_big == categoryData[0].children[i].id) {
						$("#group_category_2").empty();
						$("#group_category_2").append('<option value="请选择">-请选择-</option>');
						for(var j = 0; j < categoryData[0].children[i].children.length; j++) {
							$("#group_category_2").append('<option  value=' + categoryData[0].children[i].children[j].id + ' >' + categoryData[0].children[i].children[j].name + '</option>');
						}
					}
				}
				if(category_big != "") {
					$("#group_category_1").val(category_big);
				}
				$("#group_category_2").val(checkData[0].category);
				form.render(null, 'saveOrUpdateGroupFoodFilter');
				//套菜菜品配置
				foodGroupArray = [];
				initTable();
				getSeatMealById(checkData[0].id);

				var height = $(document).height();
				if(height > 645) {
					//$(".css-seatMeal-panel").css("height", "598px");
					$(".setMealGroupPanel").css("height", "538px")
					showModal('MODIFY-SETMEAL-MODAL', '编辑套餐', '850px', $('#saveOrUpdateSetMealPanel'), checkData[0]);
				} else {
					console.log('height:' + height);
					var str = (String)(height - 40);
					$(".setMealGroupPanel").css("height", (height - 270) + 'px');
					showModal('MODIFY-SETMEAL-MODAL', '编辑套餐', ['850px', str + 'px'], $('#saveOrUpdateSetMealPanel'), checkData[0]);
				}
			}
		},
		addFoodGroupModal: function(othis) {
			$("#saveOrUpdateFoodGroup")[0].reset()
			modalIndex = layer.open({
				type: 1,
				title: '新增分组',
				area: ['455px', '380px'],
				shade: 0.5,
				maxmin: true,
				offset: 'auto',
				id: 'ADD-FOODGROUP-MODAL' //设定一个id，防止重复弹出
					,
				moveType: 1 //拖拽模式，0或者1
					,
				content: $('#saveOrUpdateFoodGroup'),
				success: function(layero) {}
			});
		},
		addMerchantFoodGroupListEntity: function(othis) {
			getFoodList(1);
			layer.open({
				type: 1,
				title: '选择菜品',
				area: '677px',
				shade: 0.5,
				maxmin: true,
				offset: '40px',
				id: 'ADD-GROUPFOOD-MODAL' //设定一个id，防止重复弹出
					,
				moveType: 1 //拖拽模式，0或者1
					,
				content: $('#groupFoodPanel'),
				btn: ['确认', '取消'],
				success: function(layero) {},
				yes: function(mIndex, layero) {
					var foodCurrentArray = $("#js-foodList").find(".current");
					if(foodCurrentArray.length <= 0) {
						layer.msg("请先选择菜品");
						return;
					}
					var oldData = currentGroupMap.merchantFoodGroupListEntity;
					var data = [];
					$.each(foodCurrentArray, function(index, item) {
						console.log(index + ',' + item);
						var _this = $(this);
						var foodid = _this.attr('id');
						console.log(foodid);
						var foodTitle = _this.find('.foodTitleSpan').html();
						var foodName = _this.find('.foodNameSpan').html();
						var foodprice = _this.find('.foodpriceChild').html();
						var newData = {
							"id": foodid,
							"foodid": foodid,
							"name": foodName,
							//"defaultqty": "",
							"title": foodTitle,
							"category": currentGroupMap.category,
							//"isdefault": "",
							//"ismoreselect": "",
							//"addprice": "",
							"foodprice": foodprice
						}
						if(currentGroupMap.category == 0) {
							newData['defaultqty'] = 1;
							newData['isdefault'] = 1;
							newData['ismoreselect'] = 0;
						}
						if(oldData == undefined || oldData == "") {
							data.push(newData);
						} else {
							oldData.push(newData);
							data = oldData;
						}
					});
					seatMealFoodTableIns.reload({
						data: data
					});
					currentGroupMap['merchantFoodGroupListEntity'] = data;
					console.log(currentGroupMap);
					updateGroupFood(currentGroupMap);
					layer.close(mIndex);
				}
			});
		},
		closeIndexModal: function(othis) {
			layer.close(foodListModalIndex);
		},
		addAttrModal: function() {
			var checkStatus = table.checkStatus('tableSeatMealFoodList');
			var checkData = checkStatus.data;
			if(checkData.length != 1) {
				layer.msg("编辑属性只能选中一行");
				return;
			}
			if(isRequired()) {
				layer.msg("分组属性为必选的不能编辑菜品属性");
				return;
			}
			$("#js-checkbox-isinfluenceprice").attr('checked', checkData[0].isinfluenceprice == 1 ? true : false);
			$("#js-input-addprice").val(checkData[0].addprice);
			$("#js-checkbox-isdefault").attr('checked', checkData[0].isdefault == 1 ? true : false);
			$("#js-input-defaultqty").val(checkData[0].defaultqty);
			$("#js-checkbox-ismoreselect").attr('checked', checkData[0].ismoreselect == 1 ? true : false);
			$("#js-input-maximumselectqty").val(checkData[0].maximumselectqty);
			$("#foodId").val(checkData[0].id);
			foodListModalIndex = layer.open({
				type: 1,
				title: '编辑菜品属性',
				area: '455px',
				shade: 0.5,
				maxmin: true,
				offset: 'auto',
				id: 'ADD-FOODGROUPFOOD-MODAL' //设定一个id，防止重复弹出
					,
				moveType: 1 //拖拽模式，0或者1
					,
				content: $('#saveOrUpdateGroupFood'),
				success: function(layero) {}
			});

		},
		searchFoodByContent: function(othis) {
			getFoodList(1);
		},
		deleteMerchantFoodEntity: function(othis) {
			var checkStatus = table.checkStatus('tableSeatMealFoodList');
			var checkData = checkStatus.data;
			if(checkData.length <= 0) {
				layer.msg("请选择菜品进行删除");
				return;
			}
			layer.confirm('确定删除所选菜品?', {
				icon: 3,
				title: '提示'
			}, function(index) {
				let checkDataLength = checkData.length;
				console.log(checkDataLength)
				for(let i = 0; i < checkDataLength; i++) {
					let foodCheckData = checkData[i];
					console.log(foodCheckData)
					let groupfoodArray = currentGroupMap.merchantFoodGroupListEntity;
					for(let j in groupfoodArray) {
						if(foodCheckData.id == groupfoodArray[j].id) {
							groupfoodArray.splice(j, 1);
							break;
						}
					}
				}
				seatMealFoodTableIns.reload({
					data: currentGroupMap.merchantFoodGroupListEntity
				});
				console.log(currentGroupMap.merchantFoodGroupListEntity);
				updateGroupFood(currentGroupMap);
				layer.close(index);
			});
		}
	};

	/** 判断当前分组的属性是否是必选  */
	function isRequired() {
		if(currentGroupMap.category == 0) {
			return true;
		} else {
			return false;
		}
	}

	/** 更新套菜数据 */
	function updateGroupFood(groupMap) {
		for(let i in foodGroupArray) {
			let foodGroup = foodGroupArray[i];
			if(foodGroup.id == groupMap.id) {
				foodGroupArray[i] = groupMap;
			}
		}
		console.log('更新后的foodGroupArray:' + JSON.stringify(foodGroupArray));
	}

	/** 选择菜品获取菜品列表 */
	function getFoodList(curr) {
		$.ajax({
			type: "get",
			url: "/merchantfood/getfoodall",
			data: {
				search: $("#searchFood").val(),
				offset: curr,
				limit: 18
			},
			async: true,
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				console.log(data);
				let foodCount = data.count;
				if(foodCount > 0) {
					var html = '';
					var foodList = data.foodList;
					for(var i in foodList) {
						var foodEntity = foodList[i];
						if(foodEntity.isgroup == 1) {
							foodCount = foodCount - 1;
							continue;
						}
						html += '<div class="foodItemPanel fl" id="' + foodEntity.id + '"><span class="block foodNameSpan">' + foodEntity.name + '</span>';
						html += '<span class="block foodPriceSpan">￥<span class="foodpriceChild">' + foodEntity.retailPrice + '</span></span>';
						html += '<span class="foodTitleSpan none">' + foodEntity.title + '</span></div>';
					}
					$("#js-foodList").html(html);
					laypage.render({
						elem: 'pagination' //注意，这里的 test1 是 ID，不用加 # 号
							,
						count: foodCount, //数据总数，从服务端得到
						limit: 18,
						curr: location.hash.replace('#!fenye=', ''),
						hash: 'fenye',
						prev: '<i class="layui-icon" style="font-size: 15px; color: #F5711C;">&#xe603;</i>',
						next: '<i class="layui-icon" style="font-size: 15px; color: #F5711C;">&#xe602;</i>',
						theme: '#F5711C',
						jump: function(obj, first) {
							if(!first) {
								getFoodList(obj.curr);
							}
						}
					});
				} else {
					layer.msg("请先进行添加普通菜品再进行添加套餐");
				}
			}
		});
	}
	//菜单功能复用
	function foodFY() {
		//菜品分类
		$("#group_category_1").empty();
		$("#group_category_2").empty();
		for(var i = 0; i < categoryData[0].children.length; i++) {
			if($("#group_category_1").html() == null || $("#group_category_1").html() == "") { //菜品分类初始化
				$("#group_category_1").append('<option value="请选择">-请选择-</option>');
				$("#group_category_2").append('<option value="请选择">-请选择-</option>');
			}
			$("#group_category_1").append('<option value="' + categoryData[0].children[i].id + '">' + categoryData[0].children[i].name + '</option>');
		}
		form.render('select', 'saveOrUpdateGroupFoodFilter');
	}

	function showModal(id, title, area, content, row, ofs) {
		if(id == 'ADD-SETMEAL-MODAL' || id == 'MODIFY-SETMEAL-MODAL') {
			layer.open({
				type: 1,
				title: title,
				area: area,
				shade: 0.5,
				maxmin: true,
				offset: '20px',
				id: id //设定一个id，防止重复弹出
					,
				moveType: 1 //拖拽模式，0或者1
					,
				content: content,
				btn: ['确认', '取消'],
				success: function(layero) {},
				yes: function(index, layero) {
					for(var n = 0; n < $("#saveOrUpdateGroupFoodForm").find(".group-input-verify").length; n++) {
						if($("#saveOrUpdateGroupFoodForm").find(".group-input-verify:eq(" + n + ")").val() == "") {
							var value = $("#saveOrUpdateGroupFoodForm").find(".group-input-verify:eq(" + n + ")").attr("placeholder");
							layer.msg(value + "不能为空");
							return false;
						}
					}
					if($("#group_category_1").val() == "请选择" || $("#group_category_2").val() == "请选择") {
						layer.msg("菜单分类不能为空");
						return;
					}
					if(id == "MODIFY-SETMEAL-MODAL") {
						var flag = true;
						if(row.code != $("#js-input-group-code").val()) {
							$.ajax({
								type: "POST",
								url: "/merchantfood/existFood",
								data: {
									code: $("#js-input-group-code").val()
								},
								async: false,
								success: function(msg) {
									if(msg > 0) {
										flag = false;
									}
								}
							});
						}
						if(!flag) {
							layer.msg("已存在该菜品编号");
							return false;
						}
					}
					var costNum = new Number($("#js-input-group-cost").val()).toFixed(2);
					var retailPriceNum = new Number($("#js-input-group-retailPrice").val()).toFixed(2);
                    var vipPriceNum = new Number($("#js-input-group-vipPrice").val()).toFixed(2);
                    if((vipPriceNum - retailPriceNum) > 0) {
                        layer.msg("会员价不能大于零售价");
                        return false;
                    }
					//是否称重
					var isweigh = $("input[name='groupIsweigh']").siblings(".layui-form-checkbox").hasClass("layui-form-checked") == true ? 1 : 0;
					//是否禁止折扣
					var forbiddiscount = $("input[name='groupForbiddiscount']").siblings(".layui-form-checkbox").hasClass("layui-form-checked") == true ? 1 : 0;
					//是否启用开台预点
					var enablePoint = $("input[name='groupEnablePoint']").siblings(".layui-form-checkbox").hasClass("layui-form-checked") == true ? 1 : 0;
					//新增菜品数据
					var foodData = {
						"a": $("#js-input-group-code").val(), //编号
						"b": $("#js-input-group-name").val(), //名称
						"e": $("#js-input-group-pinyin").val(), //简拼
						"bar": $("#js-input-group-barcode").val(), //条形码
						"c": $("#group_category_1").val(), //大分类
						"d": $("#group_category_2").val(), //小分类
						"f": $("#js-input-group-retailPrice").val(), //零售价
						"g": $("#js-input-group-cost").val() == "" ? '0.00' : $("#js-input-group-cost").val(), //成本价
						"i": $("#js-input-group-vipPrice").val() == "" ? '0.00' : $("#js-input-group-vipPrice").val(), //会员特价
						"j": $("#js-input-group-weixin").val() == "" ? '0.00' : $("#js-input-group-weixin").val(), //扫码价
						"l": $("#js-input-group-desc").val(), //菜品介绍
						"h": Number($("#js-input-group-iscurrentPrices").val()), //时菜
						"n": isweigh, //启用称重
						"m": forbiddiscount, //禁止折扣
						"o": enablePoint, //启用开台预点
						"img": $("#js-img-group-food").attr("data-src") == undefined ? "" : $("#js-img-group-food").attr("data-src"), //图片
						"title": $("#js-input-group-title").val(),
						"status": $("#js-input-group-status").val(),
						"deduct": $("#js-input-group-deduct").val(),
						"prepareqty": $("#js-input-group-num").val() == "" ? 0 : $("#js-input-group-num").val(),
						"applyRange": applyRangeData.join()
						//"remark": $this.$content.find(".n_food textarea").val(), //备注
					};
					var foodStr = "id=" + $("#js-groupFood-id").val() + "&code=" + foodData.a + "&retailPrice=" + foodData.f + "&cost=" + foodData.g +
						"&vipPrice=" + foodData.i + "&weixin=" + foodData.j + "&category=" + foodData.d +
						"&category_big=" + foodData.c + "&name=" + foodData.b + "&iscurrentPrices=" +
						foodData.h + "&isweigh=" + foodData.n + "&forbiddiscount=" + foodData.m +
						"&deduct=" + foodData.deduct + "&remark=" + foodData.l + "&barcode=" + foodData.bar + "&avator=" +
						foodData.img + "&status=" + foodData.status + "&title=" + foodData.title +
						"&isprepare=" + foodData.o + "&applyRange=" + foodData.applyRange + "&isgroup=1";
					if(foodData.o == 1) {
						foodStr += "&prepareway=" + $("#groupEnablePointType").val();
						foodStr += "&prepareqty=" + foodData.prepareqty
					}
					if(id == "ADD-SETMEAL-MODAL") {
						var url = "/merchantfood/add";
						food(foodStr, url, index, null);
					} else {
						var url = "/merchantfood/editfood";
						food(foodStr, url, index, row);
					}
				}
			});
		} else {
			layer.open({
				type: 1,
				title: title,
				area: area,
				shade: 0.5,
				maxmin: true,
				offset: '20px',
				id: id //设定一个id，防止重复弹出
					,
				moveType: 1 //拖拽模式，0或者1
					,
				content: content,
				success: function(layero) {}
			});
		}
	}

	/* 添加/修改套菜调的接口 */
	function food(foodStr, url, modalIndex, row) {
		console.log('foodStr:' + foodStr);
		console.log('foodGroupArray:' + JSON.stringify(foodGroupArray));
		//return;
		$.ajax({
			type: "POST",
			url: url,
			data: foodStr,
			success: function(msg) {
				console.log(msg);
				if(url == '/merchantfood/add') {
					for(var i in foodGroupArray) {
						var groupEntity = foodGroupArray[i];
						if(isExistValInObj(groupEntity, 'id')) {
							delete groupEntity.id;
						}
						groupEntity['foodid'] = msg.id;
						var merchantFoodGroupList = groupEntity.merchantFoodGroupListEntity;
						for(var j in merchantFoodGroupList) {
							let merchantFoodGroup = merchantFoodGroupList[j];
							if(isExistValInObj(merchantFoodGroup, 'id')) {
								delete merchantFoodGroup.id;
							}
							if(isExistValInObj(merchantFoodGroup, 'LAY_TABLE_INDEX')) {
								delete merchantFoodGroup.LAY_TABLE_INDEX;
							}
							if(isExistValInObj(merchantFoodGroup, 'name')) {
								delete merchantFoodGroup.name;
							}
							if(isExistValInObj(merchantFoodGroup, 'title')) {
								delete merchantFoodGroup.title;
							}
							if(isExistValInObj(merchantFoodGroup, 'category')) {
								delete merchantFoodGroup.category;
							}
							if(isExistValInObj(merchantFoodGroup, 'LAY_CHECKED')) {
								delete merchantFoodGroup.LAY_CHECKED;
							}
						}
					}
					console.log('newFoodGroupArray:' + JSON.stringify(foodGroupArray));
					var groupParam = {
						'parentList': foodGroupArray
					};
					var groupUrl = '/FoodGroup/insertFoodGroup';
					$.ajax({
						type: "post",
						url: groupUrl,
						data: JSON.stringify(groupParam),
						contentType: 'application/json;charset=utf-8',
						success: function(result) {
							console.log('result:' + result);
							if(result == 'success') {
								layer.close(modalIndex);
								console.log('已进入');
								window.location.href = 'merchantFoodList.html';
							}
						}
					});
				} else {
					for(var i in foodGroupArray) {
						var groupEntity = foodGroupArray[i];
						if(isExistValInObj(groupEntity, 'id')) {
							delete groupEntity.id;
						}
						var merchantFoodGroupList = groupEntity.merchantFoodGroupListEntity;
						for(var j in merchantFoodGroupList) {
							let merchantFoodGroup = merchantFoodGroupList[j];
							if(isExistValInObj(merchantFoodGroup, 'id')) {
								delete merchantFoodGroup.id;
							}
							if(isExistValInObj(merchantFoodGroup, 'LAY_TABLE_INDEX')) {
								delete merchantFoodGroup.LAY_TABLE_INDEX;
							}
							if(isExistValInObj(merchantFoodGroup, 'name')) {
								delete merchantFoodGroup.name;
							}
							if(isExistValInObj(merchantFoodGroup, 'title')) {
								delete merchantFoodGroup.title;
							}
							if(isExistValInObj(merchantFoodGroup, 'category')) {
								delete merchantFoodGroup.category;
							}
							if(isExistValInObj(merchantFoodGroup, 'LAY_CHECKED')) {
								delete merchantFoodGroup.LAY_CHECKED;
							}
						}
					}
					console.log('newFoodGroupArray:' + JSON.stringify(foodGroupArray));
					var groupParam = {
						'parentList': foodGroupArray
					};
					var groupUrl = '/FoodGroup/updateFoodGroup';
					$.ajax({
						type: "post",
						url: groupUrl,
						data: JSON.stringify(groupParam),
						contentType: 'application/json;charset=utf-8',
						success: function(result) {
							console.log('result:' + result);
							if(result == 'success') {
								layer.close(modalIndex);
								console.log('已进入');
								window.location.href = 'merchantFoodList.html';
							}
						}
					});
				}

			}
		});
	}

	//获取套菜数据
	function getSeatMealById(id) {
		$.ajax({
			type: "get",
			url: "/FoodGroup/getFoodGroup",
			data: {
				foodId: id
			},
			async: true,
			success: function(res) {
				console.log(res);
				/*currentGroupMap = res[0];*/
				//foodGroupArray = res;
				var html = '';
				for(var i in res) {
					foodGroupMap = res[i];
					let merchantFoodGroupListEntity = foodGroupMap.merchantFoodGroupListEntity;
					for(let j in merchantFoodGroupListEntity) {
						let merchantFoodGroupEntity = merchantFoodGroupListEntity[j];
						merchantFoodGroupEntity['id'] = merchantFoodGroupEntity.foodid;
						$.ajax({
							type: "POST",
							url: "/merchantfood/getFoodById",
							data: {
								id: merchantFoodGroupEntity.foodid
							},
							async: false,
							success: function(res) {
								merchantFoodGroupEntity['name'] = res.name;
								merchantFoodGroupEntity['title'] = res.title;
							}
						});
						merchantFoodGroupEntity['category'] = foodGroupMap.category;
					}
					html += '<li class="layui-nav-item" id="' + foodGroupMap.id + '">';
					if(foodGroupMap.category == 0) {
						html += '<a href="javascript:;">' + foodGroupMap.name + '（必选）</a></li>';
					} else if(foodGroupMap.category == 1) {
						html += '<a href="javascript:;">' + foodGroupMap.name + '（单选）</a></li>';
					} else if(foodGroupMap.category == 2) {
						html += '<a href="javascript:;">' + foodGroupMap.name + '（多选）</a></li>';
					}
				}
				foodGroupArray = res;
				currentGroupMap = foodGroupArray[0];
				seatMealFoodTableIns.reload({
					data: currentGroupMap.merchantFoodGroupListEntity
				});
				$("#js-ul-group").html(html);
				element.render('nav', 'groupFilter');
			}
		});
	}

	function initTable() {
		seatMealFoodTableIns = table.render({
			elem: '#js-table-seatMealFood',
			width: 850 / 6 * 5,
			skin: 'nob',
			id: 'tableSeatMealFoodList',
			cols: [
				[ //表头
					{
						type: 'checkbox'
					}, {
						field: 'name',
						title: '菜品名称',
						width: 150,
					}, {
						field: 'defaultqty',
						title: '数量',
						width: 60,
					}, {
						field: 'title',
						title: '单位',
						width: 60,
					}, {
						field: 'category',
						title: '必选',
						width: 60,
						templet: function(d) {
							if(d.category == 0) {
								return "是";
							} else {
								return "否";
							}
						}
					}, {
						field: 'isdefault',
						title: '默认选中',
						width: 90,
						templet: function(d) {
							if(d.isdefault == 0) {
								return "否";
							} else if(d.isdefault == 1) {
								return "是";
							} else {
								return ""
							}
						}
					},
					{
						field: 'ismoreselect',
						title: '可多选',
						width: 80,
						templet: function(d) {
							if(d.ismoreselect == 0) {
								return "否";
							} else if(d.ismoreselect == 1) {
								return "是";
							} else {
								return ""
							}
						}
					}, {
						field: 'addprice',
						title: '影响价格',
					}
				]
			]
		});
	}

	$('#js-body').on('click', '.optSeatMealBtn', function() {
		var method = $(this).data('method');
		sealMealActive[method] ? sealMealActive[method].call(this) : '';
	});
});