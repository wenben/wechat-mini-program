layui.use(['table', 'form', 'element', 'upload'], function() {
	var $ = layui.$, element = layui.element;
	let rank = $("#js-data-type1").find(".layui-this").attr("data-id");
	let dateType = $("#js-data-type2").find(".layui-this").attr("data-id");
	element.on('tab(tabChangeFilter)', function(data){
		console.log(this); //当前Tab标题所在的原始DOM元素
		console.log(data.index); //得到当前Tab的所在下标
		console.log(data.elem); //得到当前的Tab大容器
		rank = $("#js-data-type1").find(".layui-this").attr("data-id");
		dateType = $("#js-data-type2").find(".layui-this").attr("data-id");
		getMerchantSell();
		initStoreCharts();
		foodSell();
		bigtypeSell();
	});
	$.ajax({
		url: '/homePage/getNowDaySellData',
		type: "post",
		async: true,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			console.log(data);
			$("#js-span-totalDisCount").html(data.data.totalDisCount);
			$("#js-span-cashierData").html(data.data.cashierData);
			$("#js-span-returnAmount").html(data.data.returnAmount);
			$("#js-span-sellData").html(data.data.sellData);
		}
	});
	$.ajax({
		url: '/homePage/getTotalInvoices',
		type: "post",
		async: true,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			$("#js-span-totalOrder").html(data.data.totalOrder);
			$("#js-span-avgPrice").html(data.data.avgPrice);
			$("#js-span-footfall").html(data.data.footfall);
			$("#js-span-rockoverProfit").html(data.data.rockoverProfit);
		}
	});
	$.ajax({
		url: '/homePage/getAveragePrice',
		type: "post",
		async: true,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			$("#js-span-dineIn").html(data.data.dineIn);
			$("#js-span-fastFood").html(data.data.fastFood);
			$("#js-span-takeOut").html(data.data.takeOut);
			$("#js-mode-span-scanCode").html(data.data.scanCode);
		}
	});
	$.ajax({
		url: '/homePage/getGrossMargin',
		type: "post",
		async: true,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			$("#js-span-cash").html(data.data.cash);
			$("#js-span-scanCode").html(data.data.scanCode);
			$("#js-span-chargeFood").html(data.data.chargeFood);
			$("#js-span-other").html(data.data.other);
		}
	});
	let width = $(document).width();
	console.log(width);
	$("#js-situation-panel").css('width', width / 10 * 8 + 'px');
	if(width > 1130) {
		$(".css-store-panel").css('width', width / 10 / 2 * 9 + 'px');
		$(".css-store-size").css('width', width / 100 * 51 + 'px');
	} else {
		$(".css-store-panel").css('width', width / 100 * 50 + 'px');
		$(".css-store-size").css('width', width / 100 * 46 + 'px');
	}
	var myChart = echarts.init(document.getElementById('js-situation-panel'));
	getMerchantSell()
	function getMerchantSell() {
		$.ajax({
			url: '/homePage/MerchantSellMsg',
			type: "post",
			data: {
				dateType: dateType
			},
			async: true,
			//contentType: "application/json;charset=utf-8",
			success: function(data) {
				let merchantSellData = data.data;
				let timeArray = [];
				let valArray = [];
				for(let i in merchantSellData) {
					let time = merchantSellData[i].timeSection;
					console.log(time);
					timeArray.push(time);
					if(rank == 0) {
						valArray.push(merchantSellData[i].totalQty);
					} else if(rank == 1) {
						valArray.push(merchantSellData[i].totalAmount);
					} else if(rank == 2) {
						valArray.push(merchantSellData[i].profit);
					}
				}
				var option = {
					xAxis: {
						type: 'category',
						boundaryGap: false,
						data: timeArray
					},
					//color: ['rgb(253, 221, 201)'],
					yAxis: {
						type: 'value'
					},
					series: [{
						data: valArray,
						type: 'line',
						symbol: 'circle',
						itemStyle: {
							normal: {
								color: "rgb(247, 138, 69)",
								lineStyle: {
									color: "rgb(247, 138, 69)"
								}
							}
						},
						areaStyle: {},
						// 高亮样式。
						emphasis: {
							label: {
								show: true,
								// 高亮时标签的文字。
								formatter: valArray,
								color: 'rgb(247, 138, 69)'
							}
						},
						/*lineStyle: {
							color: 'rgb(247, 138, 69)',
						}*/
					}],
					textStyle: {
						color: 'rgb(154, 170, 179)'
					}
				}
				// 使用刚指定的配置项和数据显示图表。
				myChart.setOption(option);
			}
		});
	}
	
	$.ajax({
		url: '/homePage/getDayWeekMonth',
		type: "post",
		async: true,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			console.log(data);
			$("#js-today").html(data.data.dayAmount);
			$("#js-yesterday").html(data.data.yesterdayAmount);
			$("#js-week").html(data.data.weekAmount);
			$("#js-month").html(data.data.monthAmount);
		}
	});
	
	initStoreCharts();
	function initStoreCharts() {
		//门店占比图
		var myChart2 = echarts.init(document.getElementById('js-store-panel'));
		$.ajax({
			url: '/homePage/getShopSellMsg',
			type: "post",
			data: {
				dateType: parseInt(dateType),
				rank: parseInt(rank)
			},
			async: true,
			success: function(data) {
				console.log(data);
				let rankText = '';
				if(rank == "0") {
					rankText = '数量';
				} else if(rank == "1") {
					rankText = '金额';
				} else if(rank == "2") {
					rankText = '毛利';
				}
				let shopSellData = data.data;
				let shopArray = [];
				let shopSellValue = [];
				let html = '';
				html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1"><span class="p10"></span></div>';
				html += '<div class="fl css-shopNum-panel2 coldg">销售'+rankText+'</div><div class="fl css-shopNum-panel2 coldg">'+rankText+'占比</div></div>';
				for(let i in shopSellData) {
					let shopSell = shopSellData[i];
					shopArray.push(shopSell.shopName);
					let sellMap = {
						name: shopSell.shopName
					};
					html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1"><span class="p10">'+shopSell.shopName+'</span></div>';
					if(rank == "0") {
						sellMap['value'] = shopSell.totalQty;
						html += '<div class="fl css-shopNum-panel2">'+shopSell.totalQty+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+shopSell.qtyRatio+'</div></div>';
					} else if (rank == "1") {
						sellMap['value'] = shopSell.totalAmount;
						html += '<div class="fl css-shopNum-panel2">'+shopSell.totalAmount+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+shopSell.amountRatio+'</div></div>';
					} else if (rank == "2") {
						sellMap['value'] = shopSell.profit;
						html += '<div class="fl css-shopNum-panel2">'+shopSell.profit+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+shopSell.profitRatio+'</div></div>';
					}
					shopSellValue.push(sellMap);
				}
				$("#js-situation-table").html(html);
				option = {
					/*tooltip: {
						trigger: 'item',
						formatter: "{a} <br/>{b}: {c} ({d}%)"
					},*/
					legend: {
						orient: 'vertical',
						x: 'right',
						data: shopArray
					},
					color: ['rgb(90, 169, 236)', 'rgb(120, 193, 61)', 'rgb(255, 131, 52)', 'rgb(251, 157, 20)'],
					series: [{
						name: '门店占比',
						type: 'pie',
						radius: ['50%', '70%'],
						avoidLabelOverlap: false,
						label: {
							normal: {
								show: true,
								//position: 'center'
							},
							emphasis: {
								//show: true,
								textStyle: {
									fontSize: '30',
									fontWeight: 'bold'
								}
							}
						},
						labelLine: {
							normal: {
								show: true
							}
						},
						data: shopSellValue,
					}]
				}
				myChart2.setOption(option);
			}
		});
	}
	var myChart3 = echarts.init(document.getElementById('js-bigtype-panel'));
	foodSell()
	//菜品前三
	function foodSell() {
		$.ajax({
			url: '/homePage/foodSellRank',
			type: "post",
			data: {
				dateType: parseInt(dateType),
				rank: parseInt(rank)
			},
			async: true,
			success: function(data) {
				console.log(data);
				let rankText = '';
				if(rank == "0") {
					rankText = '数量';
				} else if(rank == "1") {
					rankText = '金额';
				} else if(rank == "2") {
					rankText = '毛利';
				}
				let foodSellArray = data.data;
				let html = '';
				html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1"><span class="p10"></span></div>';
				html += '<div class="fl css-shopNum-panel2 coldg">销售'+rankText+'</div><div class="fl css-shopNum-panel2 coldg">'+rankText+'占比</div></div>';
				for(let i in foodSellArray) {
					html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1 pr">';
					let foodSellData = foodSellArray[i];
					if(i > 2) {
						html += '<span class="inline-block pa span-bc"></span>';
						html += '<span class="p10 pl35">'+foodSellData.foodName+'</span>';
						html += '</div>';
						if(rank == "0") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.foodQty+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.qtyRatio+'</div>';
						} else if(rank == "1") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.foodAmount+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.amountRatio+'</div>';
						} else if(rank == "2") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.profit+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.profitRatio+'</div>';
						}
						html += '</div>';
					} else {
						$("#js-foodNo"+i+"-span").html(foodSellData.foodName);
						if(rank == "0") {
							$("#js-foodNo"+i+"-qtyspan").html(rankText+"："+foodSellData.foodQty);
							$("#js-foodNo"+i+"-ratiospan").html("占比："+foodSellData.qtyRatio);
						} else if(rank == "1") {
							$("#js-foodNo"+i+"-qtyspan").html(rankText+"："+foodSellData.foodAmount);
							$("#js-foodNo"+i+"-ratiospan").html("占比："+foodSellData.amountRatio);
						} else if(rank == "2") {
							$("#js-foodNo"+i+"-qtyspan").html(rankText+"："+foodSellData.profit);
							$("#js-foodNo"+i+"-ratiospan").html("占比："+foodSellData.profitRatio);
						}
						if(i == 0) {
							html += '<span class="bc1-span inline-block pa span-bc"></span>';
						} else if(i == 1) {
							html += '<span class="bc2-span inline-block pa span-bc"></span>';
						} else {
							html += '<span class="bc3-span inline-block pa span-bc"></span>';
						}
						html += '<span class="p10 pl35">'+foodSellData.foodName+'</span>';
						html += '</div>';
						if(rank == "0") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.foodQty+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.qtyRatio+'</div>';
						} else if(rank == "1") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.foodAmount+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.amountRatio+'</div>';
						} else if(rank == "2") {
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.profit+'</div>';
							html += '<div class="fl css-shopNum-panel2">'+foodSellData.profitRatio+'</div>';
						}
						html += '</div>';
					}
				}
				$("#js-foodNo-panel").html(html);
			}
		});
	}
	bigtypeSell()
	//大类销售
	function bigtypeSell() {
		$.ajax({
			url: '/homePage/categorySellRank',
			type: "post",
			data: {
				dateType: parseInt(dateType),
				rank: parseInt(rank)
			},
			async: true,
			success: function(data) {
				console.log(data);
				let rankText = '';
				if(rank == "0") {
					rankText = '数量';
				} else if(rank == "1") {
					rankText = '金额';
				} else if(rank == "2") {
					rankText = '毛利';
				}
				let categorySellArray = data.data;
				let categoryArray = [];
				let categoryValArray = [];
				let html = '';
				html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1"><span class="p10"></span></div>';
				html += '<div class="fl css-shopNum-panel2 coldg">销售'+rankText+'</div>';
				html += '<div class="fl css-shopNum-panel2 coldg">'+rankText+'占比</div></div>';
				for(let i in categorySellArray) {
					let categorySellData = categorySellArray[i];
					categoryArray.push(categorySellData.categoryName);
					let categoryValMap = {
						name: categorySellData.categoryName
					};
					html += '<div class="borb1 oh h38"><div class="fl css-shopNum-panel1"><span class="p10">'+categorySellData.categoryName+'</span></div>';
					if(rank == "0") {
						categoryValMap['value'] = categorySellData.categoryQty;
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.categoryQty+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.qtyRatio+'</div></div>';
					} else if(rank == "1") {
						categoryValMap['value'] = categorySellData.categoryAmount;
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.categoryAmount+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.amountRatio+'</div></div>';
					} else if(rank == "2") {
						categoryValMap['value'] = categorySellData.profit;
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.profit+'</div>';
						html += '<div class="fl css-shopNum-panel2">'+categorySellData.profitRatio+'</div></div>';
					}
					categoryValArray.push(categoryValMap);
				}
				$("#js-categoryData").html(html);
				option = {
					legend: {
						// orient: 'vertical',
						// top: 'middle',
						bottom: 10,
						left: 'center',
						data: categoryArray
					},
					color: ['rgb(255, 131, 52)', 'rgb(120, 193, 61)', 'rgb(90, 169, 236)', 'rgb(155, 128, 239)', 'rgb(251, 157, 20)'],
					series: [{
						type: 'pie',
						radius: '65%',
						center: ['50%', '50%'],
						selectedMode: 'single',
						data: categoryValArray,
						itemStyle: {
							emphasis: {
								shadowBlur: 10,
								shadowOffsetX: 0,
								shadowColor: 'rgba(0, 0, 0, 0.5)'
							}
						}
					}]
				};
				myChart3.setOption(option);
				
			}
		});
	}
});