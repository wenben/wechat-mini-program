﻿//服务器地址函数
function getServerUrl(url) {
	return "http://113.105.152.179:8080" + url;
	//return "http://127.0.0.1:8020/backend" + url;
}
//转换时间工具函数
function setDateFormat(jsonDate) {
	if(jsonDate == null) {
		return "";
	}

	//json日期格式转换为正常格式
	var jsonDateStr = jsonDate.toString(); //此处用到toString（）是为了让传入的值为字符串类型，目的是为了避免传入的数据类型不支持.replace（）方法
	try {
		var k = parseInt(jsonDateStr.replace("/Date(", "").replace(")/", ""), 10);
		if(k < 0)
			return null;

		var date = new Date(parseInt(jsonDateStr.replace("/Date(", "").replace(")/", ""), 10));
		var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
		var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
		var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
		var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
		var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
		var milliseconds = date.getMilliseconds();
		return date.getFullYear() + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
	} catch(ex) {
		return "时间格式转换错误";
	}
}

function timestampToTime(timestamp) {
	var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = date.getDate();
	//	h = date.getHours() + ':';
	//	m = date.getMinutes() + ':';
	//	s = date.getSeconds();
	return Y + M + D;
}
//日期转时间戳
function timeToTimestamp(time) {
	var date = new Date(time);
	return Date.parse(date);
}

/****
 * 验证手机号
 * @param {Object} mobile
 */
function checkMobile(mobile) {
	if(!(/^1[0-9]{10}$/.test(mobile))) {
		return false;
	}
	return true;
}

/****
 * 验证是否是n位数字
 * @param {Object} value
 */
function checkNumberAndSize(value) {
	if(!(/^\d{1,9}$/.test(value))) {
		return false;
	}
	return true;
}

/***
 * 验证只能输入数字和小数点
 * @param {Object} e
 */
function checkIntAndPoint(val) {
	var re = /^\d+(\.\d+)?$/;
	if(val != "") {
		console.log(re.test(val));
		if(!re.test(val)) {
			return false;
		} else {
			return true;
		}
	}
}

/***
 * 获取商户标识
 */
var $;
layui.use(['table', 'form', 'laydate'], function() {
	$ = layui.$;
});

function getMerchantId() {
	//	$.post("/member/getUserMerchantId", function(data) {
	//		console.log("结果：" + data.msg);
	//		return data.msg;
	//	},'JSON')
	var merchantId;
	$.ajax({
		url: '/member/getUserMerchantId',
		type: "POST",
		async: false,
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			//回显
			merchantId = data.msg;
		}
	});
	return merchantId;
}

/***
 * 在对象中是否存在某字段
 * @param {Object} obj
 * @param {Object} val
 */
function isExistValInObj(obj, val) {
	if(val in obj) {
		return true;
	} else {
		return false;
	}
}

/***
 * 数组是否为空
 * @param {Object} array
 */
function isNullArray(array) {
	if(array == null || array == undefined || array.length <= 0) {
		return true;
	} else {
		return false;
	}
}

/*
 * fmt:时间格式
 */
Date.prototype.pattern = function(fmt) {
	var o = {
		"M+": this.getMonth() + 1, // 月份
		"d+": this.getDate(), // 日
		"h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, // 小时
		"H+": this.getHours(), // 小时
		"m+": this.getMinutes(), // 分
		"s+": this.getSeconds(), // 秒
		"q+": Math.floor((this.getMonth() + 3) / 3), // 季度
		"S": this.getMilliseconds()
		// 毫秒
	};
	var week = {
		"0": "/u65e5",
		"1": "/u4e00",
		"2": "/u4e8c",
		"3": "/u4e09",
		"4": "/u56db",
		"5": "/u4e94",
		"6": "/u516d"
	};
	if(/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
			.substr(4 - RegExp.$1.length));
	}
	if(/(E+)/.test(fmt)) {
		fmt = fmt
			.replace(
				RegExp.$1,
				((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f" :
						"/u5468") :
					"") +
				week[this.getDay() + ""]);
	}
	for(var k in o) {
		if(new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) :
				(("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}